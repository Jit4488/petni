﻿using System;
namespace PETni.Business
{
    public class Enums
    {
        public enum KeyBox
        {
            UserId = 1,
            UserEmail = 2,
            IsSignedUP = 3,
            EmailVerificationToken = 4
        }

        public enum RegisterType
        {
            normal = 0,
            fb = 1,
            gplus = 2
        }
    }
}
