﻿using System;
using System.Text.RegularExpressions;

namespace PETni.Business
{
    public static class Helpers
    {
        public static bool IsEmpty(this string str)
        {
            return String.IsNullOrEmpty(str) || String.IsNullOrWhiteSpace(str);
        }

        public static bool ValidateEmail(this string email)
        {
            const string MatchEmailPattern = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
            var res = Regex.IsMatch(email, MatchEmailPattern);
            return res;
        }

        public static string GetUniqueFileName()
        {
            return DateTime.Now.ToString("yyyyMMddhhmmssffff");
        }
    }
}