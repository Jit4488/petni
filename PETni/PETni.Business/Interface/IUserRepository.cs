﻿using System;
using System.Threading.Tasks;

namespace PETni.Business
{
    public interface IUserRepository
    {
        Task<LoginResponse> Login(LoginRequest request);
    }
}
