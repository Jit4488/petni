﻿using System;
using System.Threading.Tasks;

namespace PETni.Business
{
    public class UserRepository : IUserRepository
    {
        readonly HttpRequestHelper client;
        public UserRepository()
        {
            this.client = new HttpRequestHelper();
        }

        public async Task<LoginResponse> Login(LoginRequest request)
        {
            return await this.client.CreatePostRequest<LoginResponse>("login", request);
        }
    }
}
