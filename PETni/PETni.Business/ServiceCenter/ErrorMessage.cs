﻿using System;
namespace PETni.Business
{
    public class ErrorMessage
    {
        public ErrorMessage()
        {
        }

        public string Message { get; set; }

        public string StackTrace { get; set; }
    }
}
