﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace PETni.Business
{
    public class GoogleServices
    {
        public static readonly string ClientId = "10799725651-4gludutdjuoel35u3mhevpev0upeajrv.apps.googleusercontent.com";
        public static readonly string ClientSecret = "b98FyGBgTIprqPoFjj_hY_7i";
        public static readonly string RedirectUri = "https://latitudetechnolabs.com/";

        public async Task<string> GetAccessTokenAsync(string code)
        {
            var httpClient = new HttpClient();

            httpClient.BaseAddress = new Uri("https://www.googleapis.com/oauth2/v4/token");
            var requestParameter = "code=" + code + "&client_id=" + ClientId + "&client_secret=" + ClientSecret + "&redirect_uri=" + RedirectUri + "&grant_type=authorization_code" + "&scopes=https://www.googleapis.com/auth/userinfo.email";
            StringContent str = new StringContent(requestParameter, Encoding.UTF8, "application/x-www-form-urlencoded");


            var response = await httpClient.PostAsync(new Uri("https://www.googleapis.com/oauth2/v4/token"), str);

            var json = await response.Content.ReadAsStringAsync();

            var accessToken = JsonConvert.DeserializeObject<JObject>(json).Value<string>("access_token");
            var Idtoken = JsonConvert.DeserializeObject<JObject>(json).Value<string>("id_token");

            return accessToken;
        }

        public async Task<GoogleProfileResponce> GetGoogleUserProfileAsync(string accessToken)
        {

            var requestUrl = "https://www.googleapis.com/plus/v1/people/me"
                             + "?access_token=" + accessToken;

            var httpClient = new HttpClient();

            var userJson = await httpClient.GetStringAsync(requestUrl);

            var googleProfile = JsonConvert.DeserializeObject<GoogleProfileResponce>(userJson);

            return googleProfile;
        }
    }
}
