﻿using PETni.Business;
using Xamarin.Forms;

namespace PETni
{
    public partial class App : Application
    {
        #region CommonProperty
        public static IUserPreferences userPreferences = DependencyService.Get<IUserPreferences>();
        public static IUserRepository userRepository = RepositoryInitiator<IUserRepository>.Instance;
        #endregion

        #region Property
        public static INavigation Navigation { get; set; }
        public static Page CurrentPage { get; set; }
        #endregion

        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new LoginPage());
            Navigation = MainPage.Navigation;
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
