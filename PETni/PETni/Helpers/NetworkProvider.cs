﻿using System;
using PETni.Business;
using Plugin.Connectivity;

namespace PETni
{
    public static class NetworkProvider
    {
        public static bool IsAvailable()
        {
            var connectivity = CrossConnectivity.Current;
            if (!connectivity.IsConnected)
            {
                Utilities.ShowOkAlert(Constants.NetworkMessage);
            }
            return connectivity.IsConnected;
        }
    }
}
