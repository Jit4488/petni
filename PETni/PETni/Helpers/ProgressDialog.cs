﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PETni
{
    /// <summary>
    /// Progress dialog
    /// </summary>
    public static class ProgressDialog
    {
        public static void ShowProgress(String title = "Loading...")
        {
            DependencyService.Get<IProgressDialog>().Show(title);
        }

        public static void HideProgress()
        {
            DependencyService.Get<IProgressDialog>().Dismiss();
        }

        public static void ShowToastMessage(String title = "Loading...")
        {
            DependencyService.Get<IProgressDialog>().Show(title);
        }

        public static void ShowProgressWithMessage(string message, int completed, int Total)
        {
            DependencyService.Get<IProgressDialog>().ShowProgressWithMessage(message, completed, Total);
        }
    }

    /// <summary>
    /// User interface thread.
    /// </summary>
    public static class UIThread
    {
        public async static void BeginInvokeOnMainThreadAsync(Action action)
        {
            await MainThreadAsync(action);
        }

        private static Task MainThreadAsync(Action action)
        {
            TaskCompletionSource<object> tcs = new TaskCompletionSource<object>();
            Device.BeginInvokeOnMainThread(() =>
            {
                try
                {
                    action();
                    tcs.SetResult(null);
                }
                catch (Exception ex)
                {
                    tcs.SetException(ex);
                }
            });
            return tcs.Task;
        }
    }
}
