﻿using System;
using PETni.Business;

namespace PETni
{
    public interface IUserPreferences
    {
        void SetString(Enums.KeyBox key, string value);
        string GetString(Enums.KeyBox key);
    }
}
