﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PETni.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HomePage : ContentPage
	{
		public HomePage ()
		{
            try
            {
                InitializeComponent();
                this.BindingContext = new HomeViewModel();
                NavigationPage.SetHasNavigationBar(this, false);
            }
            catch (Exception)
            {

                throw;
            }
			
		}
	}
}