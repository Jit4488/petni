﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PETni.Business;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using PETni.Pages;

namespace PETni
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginWithGooglePage : ContentPage
    {
        GoogleServices objGoogleservice = new GoogleServices();
        private readonly LoginWithGoogleViewModel _loginWithgoogleViewModel = new LoginWithGoogleViewModel();
        emails objemail = new emails();

        public LoginWithGooglePage()
        {
            try
            {
                InitializeComponent();
                BindingContext = _loginWithgoogleViewModel;

                Title = "Google Profile";
                BackgroundColor = Color.White;

                var authRequest =
                      "https://accounts.google.com/o/oauth2/v2/auth"
                      + "?response_type=code"
                      + "&scope=https://www.googleapis.com/auth/userinfo.email"
                      + "&redirect_uri=" + GoogleServices.RedirectUri
                      + "&client_id=" + GoogleServices.ClientId;

                ProgressDialog.HideProgress();
                var webView = new ExtendedWebView
                {
                    Source = authRequest,
                    HeightRequest = 1
                };

                webView.Navigated += WebViewOnNavigated;
                Content = webView;
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }

        private async void WebViewOnNavigated(object sender, WebNavigatedEventArgs e)
        {

            try
            {
                var code = ExtractCodeFromUrl(e.Url);
                if (code != "")
                {
                    var accessToken = await GetAccessTokenAsync(code);
                    await Navigation.PopAsync(true);
                    await SetGoogleUserProfileAsync(accessToken);
                }
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }

        }

        public async Task<string> GetAccessTokenAsync(string code)
        {
            try
            {
                ProgressDialog.ShowProgress();
                var accessToken = await objGoogleservice.GetAccessTokenAsync(code);
                return accessToken;
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
                return null;
            }
        }

        public async Task SetGoogleUserProfileAsync(string accessToken)
        {
            try
            {
                ProgressDialog.ShowProgress();
                var GoogleProfile = await objGoogleservice.GetGoogleUserProfileAsync(accessToken);
                objemail = GoogleProfile.emails[0];
                string email = objemail.value;

                if (NetworkProvider.IsAvailable())
                {
                    ProgressDialog.ShowProgress();
                    LoginRequest req = new LoginRequest();
                    string firstName, lastName;
                    if (!string.IsNullOrEmpty(GoogleProfile.Name.GivenName))
                    {
                        firstName = GoogleProfile.Name.GivenName;
                    }
                    else
                    {
                        firstName = email.Split('@')[0];
                    }
                    if (!string.IsNullOrEmpty(GoogleProfile.Name.FamilyName))
                    {
                        lastName = GoogleProfile.Name.FamilyName;
                    }
                    else
                    {
                        lastName = email.Split('@')[0];
                    }

                    req.google_id = GoogleProfile.Id;
                    req.type = Constants.GoogleLoginType;
                    req.device_token = Constants.DeviceToken;
                    req.device_type = Constants.DeviceType;

                    var res = await App.userRepository.Login(req);
                    if (res != null)
                    {
                        if (res.Error == Constants.HTTP_REQUEST_SUCCESS)
                        {
                            ProgressDialog.HideProgress();
                            BaseViewModel.SetUserPreference(Enums.KeyBox.UserId, res.user_id.ToString());
                            await App.Navigation.PushAsync(new HomePage(), true);
                        }
                        else if (res.Error == Constants.HTTP_REQUEST_ERROR)
                        {
                            ProgressDialog.HideProgress();
                            Utilities.ShowOkAlert(res.Message);
                        }
                        else
                        {
                            ProgressDialog.HideProgress();
                            Utilities.ShowOkAlert(Constants.Error);
                        }
                    }
                    else
                    {
                        ProgressDialog.HideProgress();
                        Utilities.ShowOkAlert(Constants.responceNull);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }

        private string ExtractCodeFromUrl(string url)
        {
            try
            {
                if (url.Contains("code="))
                {
                    var attributes = url.Split('&');

                    var code = attributes.FirstOrDefault(s => s.Contains("code=")).Split('=')[1];

                    return code;
                }

                return string.Empty;
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
                return string.Empty;
            }
        }
    }
}
