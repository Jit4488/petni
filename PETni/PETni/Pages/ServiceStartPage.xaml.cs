﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PETni.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ServiceStartPage : ContentPage
	{
		public ServiceStartPage ()
		{
            try
            {
			InitializeComponent ();
             NavigationPage.SetHasNavigationBar(this, false);
             this.BindingContext = new ServiceViewModel();
            }
            catch (Exception)
            {

                throw;
            }
		}
	}
}