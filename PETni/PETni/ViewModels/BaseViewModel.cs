﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using PETni.Business;
using PETni.Pages;
using Xamarin.Forms;

namespace PETni
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        #region InotifyPropertychangeImplement
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region StaticMethods
        public static string GetUserPreference(Enums.KeyBox key)
        {
            return App.userPreferences.GetString(key);
        }

        public static void SetUserPreference(Enums.KeyBox key, string value)
        {
            App.userPreferences.SetString(key, value);
        }
        #endregion

        #region Properties
        #endregion

        #region google login
        private Command googleLoginCommand;
        public Command GoogleLoginCommand
        {
            get
            {
                return googleLoginCommand ?? new Command((obj) =>
                {
                    googleLoginCommandExecute();
                });
            }
        }

        public void googleLoginCommandExecute()
        {
            try
            {
                if (NetworkProvider.IsAvailable())
                {
                    App.Navigation.PushAsync(new LoginWithGooglePage(), true);
                }
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
            finally
            {
                ProgressDialog.HideProgress();
            }
        }
        #endregion

        #region FacebookLogin

        private bool _isLogedIn;

        private FacebookUser _facebookUser;

        public FacebookUser FacebookUser
        {
            get { return _facebookUser; }
            set { _facebookUser = value; OnPropertyChanged("FacebookUser"); }
        }

        public bool IsLogedIn
        {
            get { return _isLogedIn; }
            set { _isLogedIn = value; OnPropertyChanged("IsLogedIn"); }
        }
        private void FacebookLogout()
        {
            DependencyService.Get<IFacebookManager>().Logout();
            IsLogedIn = false;
        }

        private Command facebookLoginCommand;
        public Command FacebookLoginCommand
        {
            get
            {
                return facebookLoginCommand ?? new Command(FacebookLogin);
            }
        }

        private void FacebookLogin()
        {
            try
            {
                DependencyService.Get<IFacebookManager>().Login(OnLoginComplete);
            }
            catch (Exception ex)
            {
                ex.SendErrorLog();
            }
            finally
            {
                ProgressDialog.HideProgress();
            }
        }

        private void OnLoginComplete(FacebookUser facebookUser, string message)
        {
            try
            {
                if (facebookUser != null)
                {
                    FacebookUser = facebookUser;
                    IsLogedIn = true;
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await SocialLoginWithFacebook(facebookUser);
                    });
                }
                else
                {
                    Utilities.ShowOkAlert(message);
                }
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
            finally
            {
                ProgressDialog.HideProgress();
            }

        }

        public async Task SocialLoginWithFacebook(FacebookUser facebookUser)
        {
            try
            {
                if (NetworkProvider.IsAvailable())
                {
                    ProgressDialog.ShowProgress();

                    LoginRequest req = new LoginRequest();
                    req.facebook_id = facebookUser.Id;
                    //req.email = facebookUser.Email;
                    req.type = Constants.fbLoginType;
                    req.device_token = Constants.DeviceToken;
                    req.device_type = Constants.DeviceType;

                    var res = await App.userRepository.Login(req);
                    if (res != null)
                    {
                        if (res.Error == Constants.HTTP_REQUEST_SUCCESS)
                        {
                            ProgressDialog.HideProgress();
                            SetUserPreference(Enums.KeyBox.UserId, res.user_id.ToString());
                            await App.Navigation.PushAsync(new HomePage(), true);
                        }
                        else if (res.Error == Constants.HTTP_REQUEST_ERROR)
                        {
                            ProgressDialog.HideProgress();
                            Utilities.ShowOkAlert(res.Message);
                        }
                        else
                        {
                            ProgressDialog.HideProgress();
                            Utilities.ShowOkAlert(Constants.Error);
                        }
                    }
                    else
                    {
                        ProgressDialog.HideProgress();
                        Utilities.ShowOkAlert(Constants.responceNull);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
            finally
            {
                ProgressDialog.HideProgress();
            }
        }

        #endregion

        #region Constructors
        public BaseViewModel()
        {
        }
        #endregion

        #region Methods
        public static void RemovePreviousPage()
        {
            try
            {
                App.Navigation.RemovePage(App.Navigation.NavigationStack.ElementAt(App.Navigation.NavigationStack.Count - 2));
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }
        #endregion
    }
}