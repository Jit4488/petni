﻿using System;
using System.Collections.ObjectModel;

namespace PETni
{
    public class HomeViewModel : BaseViewModel
    {
        #region Properties
        private ObservableCollection<string> serviceList=new ObservableCollection<string>();
        public ObservableCollection<string> ServiceList
        {
            get { return serviceList; }
            set { serviceList = value; OnPropertyChanged(); }
        }

        private string _selectedItem;
        public string  SelectedItem
        {
            get{return _selectedItem;}
            set
            {
                _selectedItem = value;
                if(_selectedItem!=null)
                _selectedItem = null;
                OnPropertyChanged("SelectedItem");
            }
        }
        #endregion

        #region Commands
        #endregion

        #region Construcotr

        public HomeViewModel()
        {
            ServiceList.Add("Harsh");
            ServiceList.Add("Bhargav");
            ServiceList.Add("Jack");
            ServiceList.Add("Bilal");
            ServiceList.Add("a");
            ServiceList.Add("a");
            ServiceList.Add("a");
            ServiceList.Add("a");
        }

        #endregion

        #region Methods
        #endregion

    }
}
