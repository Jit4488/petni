﻿using System;
using System.Threading.Tasks;
using PETni.Business;
using PETni.Pages;
using Xamarin.Forms;

namespace PETni
{
    public class LoginViewModel : BaseViewModel
    {
        #region Properties
        private string email;
        public string Email
        {
            get { return email; }
            set { email = value; OnPropertyChanged(); }
        }
        private string password;
        public string Password
        {
            get { return password; }
            set { password = value; OnPropertyChanged(); }
        }
        #endregion

        #region Constructors
        public LoginViewModel()
        {
#if DEBUG
            Email = "dev.05.ittanta@gmail.com";
            Password = "12345678";
#endif
        }
        #endregion

        #region Commands

        private Command loginCommand;
        public Command LoginCommand
        {
            get
            {
                return loginCommand ?? new Command((obj) =>
                {
                    Login();
                });
            }
        }

        #endregion

        #region Methods

        public async Task Login()
        {
            try
            {
                if (Email.IsEmpty())
                {
                    Utilities.ShowOkAlert("Email is required");
                    return;
                }
                if (!Helpers.ValidateEmail(Email))
                {
                    Utilities.ShowOkAlert("Please enter a valid email");
                    return;
                }
                if (Password.IsEmpty())
                {
                    Utilities.ShowOkAlert("Please insert password");
                    return;
                }
                if (NetworkProvider.IsAvailable())
                {
                    ProgressDialog.ShowProgress();

                    LoginRequest req = new LoginRequest();
                    req.email = Email;
                    req.password = Password;
                    req.type = Constants.Type;
                    req.device_token = Constants.DeviceToken;
                    req.device_type = Constants.DeviceType;

                    var res = await App.userRepository.Login(req);
                    if (res != null)
                    {
                        if (res.Error == Constants.HTTP_REQUEST_SUCCESS)
                        {
                            ProgressDialog.HideProgress();
                            SetUserPreference(Enums.KeyBox.UserId, res.user_id.ToString());
                            await App.Navigation.PushAsync(new HomePage(), true);
                        }
                        else if (res.Error == Constants.HTTP_REQUEST_ERROR)
                        {
                            ProgressDialog.HideProgress();
                            Utilities.ShowOkAlert(res.Message);
                        }
                        else
                        {
                            ProgressDialog.HideProgress();
                            Utilities.ShowOkAlert(Constants.Error);
                        }
                    }
                    else
                    {
                        ProgressDialog.HideProgress();
                        Utilities.ShowOkAlert(Constants.responceNull);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
            finally
            {
                ProgressDialog.HideProgress();
            }
        }
        #endregion
    }
}
