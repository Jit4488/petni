﻿using System;
using System.Threading.Tasks;
using PETni.Business;

namespace PETni
{
    public class LoginWithGoogleViewModel : BaseViewModel
    {

        private readonly GoogleServices _googleServices;

        private GoogleProfileResponce _googleProfile;
        public GoogleProfileResponce GoogleProfile
        {
            get { return _googleProfile; }
            set { _googleProfile = value; OnPropertyChanged(); }
        }

        public LoginWithGoogleViewModel()
        {
        }

        public async Task<string> GetAccessTokenAsync(string code)
        {

            var accessToken = await _googleServices.GetAccessTokenAsync(code);

            return accessToken;
        }

        public async Task SetGoogleUserProfileAsync(string accessToken)
        {
            try
            {
                GoogleProfile = await _googleServices.GetGoogleUserProfileAsync(accessToken);
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }

    }
}
