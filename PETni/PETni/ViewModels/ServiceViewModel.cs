﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace PETni
{
	public class ServiceViewModel : BaseViewModel
	{
        #region Properties
        private ObservableCollection<string> serviceList = new ObservableCollection<string>();
        public ObservableCollection<string> ServiceList
        {
            get { return serviceList; }
            set { serviceList = value; OnPropertyChanged(); }
        }

        private string _selectedItem;
        public string SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                if (_selectedItem != null)
                    _selectedItem = null;
                OnPropertyChanged("SelectedItem");
            }
        }
        #endregion

        #region Commands
        #endregion

        #region Construcotr
        public ServiceViewModel ()
		{
            ServiceList.Add("Don't for get care instruction below:");
        }
        #endregion

        #region Methods
        #endregion
    }
}