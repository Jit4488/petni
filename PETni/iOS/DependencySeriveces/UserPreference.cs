﻿using System;
using PETni.Business;
using PETni.iOS;
using Xamarin.Forms;

[assembly: Dependency(typeof(UserPreference))]
namespace PETni.iOS
{
    public class UserPreference : IUserPreferences
    {
        public UserPreference()
        {
        }

        public void SetString(Enums.KeyBox Key, string Value)
        {
            var defaults = Foundation.NSUserDefaults.StandardUserDefaults;
            if (defaults != null)
            {
                defaults.SetString(Value != null ? Value : String.Empty, Key.ToString());
                defaults.Synchronize();
                //return true;
            }
            //return false;
        }

        public string GetString(Enums.KeyBox Key)
        {
            return Foundation.NSUserDefaults.StandardUserDefaults.StringForKey(Key.ToString());
        }
    }
}
